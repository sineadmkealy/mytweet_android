package org.wit.mytweet.activities;

import java.util.Date;

import org.wit.android.helpers.ContactHelper;

import org.wit.mytweet.R;
import org.wit.mytweet.models.Portfolio;
import org.wit.mytweet.app.MyTweetApp;
import org.wit.mytweet.models.Tweet;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import static org.wit.android.helpers.IntentHelper.navigateUp;
import static org.wit.android.helpers.ContactHelper.sendEmail;


/**
 * An activity is a single, focused thing that the user can do
 * Almost all activities interact with the user, so the Activity class takes care of creating :
 * a full screen window in which you to place the UI,
 * as floating windows (via a theme with windowIsFloating set)
 * or embedded inside of another activity (ActivityGroup)
 *
 * An Activity may comprise one or more fragments, each of which represents a portion of a user interface.
 * Fragments facilitates the inclusion of a horizontal swipe feature to the TweetActivity class
 *
 * Implements the Listener interface 'TextWatcher': it has 3 methods (that must be implemented on instantiation of a class implementing Textwatcher):
 * onTextChanged, beforeTextChanged, afterTextChanged
 *//*
*/

public class TweetFragment extends Fragment implements TextWatcher, View.OnClickListener {

  public static final String EXTRA_TWEET_ID = "mytweet.TWEET_ID";
  private static final int REQUEST_CONTACT = 1;// ID  for the Contact - implicit Intent

  private Button tweetButton;
  private TextView countdown; // refers to no characters on display on screen
  private EditText tweetMessage; //EditText is a subclass of TextView
  private TextView tweetDate;
  private Button contactButton; // button object to trigger the intent
  private Button emailToButton;
  private Long date;
  private int totalChars = 140;
  private String countRemainingString;

  private String emailAddress = "";
  private String subject = "From MyTweetPager";

  private Tweet tweet;
  private Portfolio portfolio;
  MyTweetApp app;


  /**
   * To initialize the activity.
   * call setContentView(int) with a layout resource defining the UI
   * or findViewById(int) to retrieve the widgets in that UI that you need to interact with programmatically.
   *
   * Listen for changes to the TweetFragment input control:
   * When changes are detected (when a user enters data) the listener will receive the data and transmit it to the model class, Tweet
   * use the content auto-generated identifier(R.id.content) to obtain a reference to this EditText control.
   */
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);

    //extracts the current tweet id from the Bundle object that was created in the TweetPagerActivity/PagerAdapter.getItem method which is located in the file TweetPagerActivity
    // ie adds a fragment argument to the Bundle object. The value of this argument is the current tweet id.
    Long tweetId = (Long)getArguments().getSerializable(EXTRA_TWEET_ID);

    app = MyTweetApp.getApp();
    portfolio = app.portfolio;
    tweet = portfolio.getTweet(tweetId);
  }

  /**
   * If the user clicks on an item (in list), it should start the TweetActivity/Fragment -
   * Installs the TweetActivity/Fragment object as a listener.
   */
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
    super.onCreateView(inflater, parent, savedInstanceState);
    View v = inflater.inflate(R.layout.fragment_tweet, parent, false);

    //Tweet tweet = (Tweet)getActivity();
    //tweet.actionBar.setDisplayHomeAsUpEnabled(true);

    addListeners(v);
    updateControls(tweet);

    return v;
  }

  /**
   *  TweetFragement fields above  - mapped to widgets in layout
   *  Method initializes the activity and enabling the event handlers below in UpdateControls
   */
  private void addListeners(View v) {

    countdown = (TextView) v.findViewById(R.id.noCharacters); // to be defined

    //initialising the buttons;  Register a listener on the content object
    tweetButton = (Button) v.findViewById(R.id.Tweet);
    tweetButton.setOnClickListener(this); //// enabling the event handlers

    //Register a TextWatcher in the EditText content object
    tweetMessage = (EditText) v.findViewById(R.id.tweetMessage);// Register a TextWatcher in the EditText content object
    tweetMessage.addTextChangedListener(this);

    tweetDate = (TextView) v.findViewById(R.id.tweet_date);
    date = new Date().getTime();
   // tweetDate.setText(tweet.getDateString());

    contactButton = (Button) v.findViewById(R.id.selectContact);
    contactButton.setOnClickListener(this);

    emailToButton = (Button) v.findViewById(R.id.emailTweet);
    emailToButton.setOnClickListener(this);
  }

  /**
   * to send the tweet data to the view widgets
   */
  public void updateControls(Tweet tweet) {
    tweetMessage.setText(tweet.content); //...?????
    tweetDate.setText(tweet.getDateString());
  }

  /***
   * Menu 'Up button'
   */
  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        navigateUp(getActivity());
        return true;
    }
    return super.onOptionsItemSelected(item);
  }

  /**
   * ‘onPause’ event:
   * Triggers a save when the user leaves the TweetActivity/Fragment, ensuring that data is actually saved
   */
  @Override
  public void onPause() {
    super.onPause();
    portfolio.saveTweets();
  }

  //------------------------------------------------------------------------------------------------
  /**
   * Auto-generated method for implementation of TextWatcher interface
   * Not relevant but must be implemented as wrappers only to avoid compile time error
   */
  @Override
  public void beforeTextChanged(CharSequence s, int start, int count, int after) {
  }

  /**
   * Records the tweet message content, activates the 140 character countdown
   */

  @Override
  public void onTextChanged(CharSequence s, int start, int before, int count) {
    int messageCharacters = s.length();
    int countRemaining = (totalChars - messageCharacters);
    countRemainingString = Integer.toString(countRemaining);
    countdown.setText(countRemainingString);

    if (tweetMessage.isFocused() && tweetMessage.getText().toString().trim().length() > totalChars) {
      tweetMessage.setText(s.toString().substring(0, totalChars));
      tweetMessage.setSelection(messageCharacters - 1);

      Toast toast = Toast.makeText(getActivity(), "The maximum no. of characters reached", Toast.LENGTH_SHORT);
      toast.show();
    }
  }

  /**
   * Auto-generated method for implementation of 'TextWatcher interface
   * Updates content registered at onTextChanged above
   */
  @Override
  public void afterTextChanged(Editable s) {
    tweet.setContent(s.toString());
  }
  //---------------------------------------------------------------------------------------------------------------
  /**
   * A 'startActivityForResult' method to recover the actual contact
   */
  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (resultCode != Activity.RESULT_OK) {
      return;
    }

    switch (requestCode) {
      case REQUEST_CONTACT:
        String name = ContactHelper.getContact(getActivity(), data);
        emailAddress = ContactHelper.getEmail(getActivity(), data);
        contactButton.setText(name + " : " + emailAddress);
        //tweet.contact = name;
        break;
    }
  }
  //------------------------------------------------------------------------------------------------
  /**
   * Event handlers for Tweet message, Contact and sendEmail
   * Auto-generated method for associated interface
   */
  @Override
  public void onClick(View view) {
    switch (view.getId()) {
      case R.id.Tweet:
       tweet = new Tweet();
        tweet.content = tweetMessage.getText().toString();
        Log.v("tweetMessage", tweet.content);

        if (tweetMessage.getText().length() > 0) {
          //portfolio.addTweet(tweet); // already doing this in onOptionsItemSelected(MenuItem item) {
          //portfolio.saveTweets();
          startActivity(new Intent(getActivity(), TweetListActivity.class));
          Toast toast = Toast.makeText(getActivity(), "Message Sent", Toast.LENGTH_SHORT);
          toast.show();
        }
        //else {
         // startActivity(new Intent(getActivity(), TweetListActivity.class));
        //}
        break;

      case R.id.selectContact:
        Intent i = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(i, REQUEST_CONTACT); //action described above in onActivityResult
        if (tweet.contact != null)
        {
          contactButton.setText(emailAddress);
        }
        break;

      case R.id.emailTweet: // calls the ContactHelper
        sendEmail(getActivity(), emailAddress, getString(R.string.tweet_subject), tweetMessage.getText().toString());

        break;
    }
  }


}




