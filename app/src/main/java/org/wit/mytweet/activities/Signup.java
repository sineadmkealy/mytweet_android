package org.wit.mytweet.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import org.wit.mytweet.R;
import org.wit.mytweet.app.MyTweetApp;
import org.wit.mytweet.models.User;

/**
 * Signup class implements a listener interface for the OnClick events.
 */
public class Signup extends AppCompatActivity implements View.OnClickListener {

  private User user;

  private TextView firstName;
  private TextView lastName;
  private TextView email;
  private TextView password;
  private Button signupButton;
  //private MyTweetApp app;

  /**
   *  Method is called when an instance of the activity subclass is created
   */
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    //inflates a layout and puts it on screen. When a layout is inflated,
    //each widget in the layout file (resource ID) is instantiated as defined by its attributes
    setContentView(R.layout.activity_signup);

    signupButton = (Button) findViewById(R.id.signup);
    signupButton.setOnClickListener(this);

  }

  /**
   * If the user clicks on an item (in list), it starts the relevant Activity view.
   * Uses UML 'Roles' 'Association' Class Relationship type:
   * The association is specifically named - the attribute name 'email' maps to the associated widget
   */
  @Override
  public void onClick(View view) {

    firstName = (TextView) findViewById(R.id.firstName);
    lastName = (TextView) findViewById(R.id.lastName);
    email = (TextView) findViewById(R.id.Email);
    password = (TextView) findViewById(R.id.Password);

    User user = new User(firstName.getText().toString(), lastName.getText().toString(), email.getText().toString(), password.getText().toString());

    //Log.v("check", app.users.get(0).firstName);
    startActivity(new Intent(this, Login.class));

    MyTweetApp app = (MyTweetApp) getApplication();
    app.newUser(user);

    Toast toast = Toast.makeText(this, "Registered for MyTweet!", Toast.LENGTH_SHORT);
    toast.show();

  }
}




