package org.wit.mytweet.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import org.wit.mytweet.R;
import org.wit.mytweet.app.MyTweetApp;

/**
 * Welcome class implements a listener interface for the OnClick events.
 */
public class Welcome extends AppCompatActivity implements View.OnClickListener {

    private Button signupButton;
    private Button loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        MyTweetApp app = MyTweetApp.getApplication();

        signupButton = (Button)  findViewById(R.id.welcomeSignup);
        loginButton = (Button)  findViewById(R.id.welcomeLogin);

        signupButton.setOnClickListener(this);
        loginButton.setOnClickListener(this);
    }

  /**
   * If the user clicks on an item (in list), it starts the relevant Activity view.
   * Uses UML 'Roles' 'Association' Class Relationship type:
   * The association is specifically named - the attribute name 'signupButton' maps to the associated widget 'welcomeSignup'.
   */
    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.welcomeSignup :
                startActivity(new Intent(this, Signup.class));
                break;
            case R.id.welcomeLogin :
                startActivity(new Intent(this, Login.class));
                break;
        }
    }

}
