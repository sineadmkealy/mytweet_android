package org.wit.mytweet.activities;


import org.wit.mytweet.R;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

/**
 * TweetListActivity - to display the full portfolio of tweets
 * An Activity may comprise one or more fragments, each of which represents a portion of a user interface.
 * Fragments facilitates the inclusion of a horizontal swipe feature to the TweetActivity class
 */
public class TweetListActivity extends AppCompatActivity
{
  public void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.fragment_container);

    FragmentManager manager = getSupportFragmentManager();
    Fragment fragment = manager.findFragmentById(R.id.fragmentContainer);
    if (fragment == null)
    {
      fragment = new TweetListFragment();
      manager.beginTransaction().add(R.id.fragmentContainer, fragment).commit();
    }
  }
}
