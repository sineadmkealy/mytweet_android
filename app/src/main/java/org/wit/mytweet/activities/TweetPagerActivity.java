package org.wit.mytweet.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import org.wit.mytweet.R;
import org.wit.mytweet.app.MyTweetApp;
import org.wit.mytweet.models.Portfolio;
import org.wit.mytweet.models.Tweet;

import java.util.ArrayList;
import static org.wit.android.helpers.LogHelpers.info;

/**
 * Uses the Android ViewPager layout manager 'PageAdapter' abstract class to support horizontal 'swipe' interaction.
 * Allows a user to page through the entire tweet list, beginning from within any details view, in either direction.
 */
public class TweetPagerActivity extends AppCompatActivity  implements ViewPager.OnPageChangeListener
{
    private ViewPager viewPager;
    private ArrayList<Tweet> tweets; // instance variables for a list of tweets and portfolio
    private Portfolio portfolio;     //  to facilitate retrieval of views that are required as a user swipes left or right.

    private PagerAdapter pagerAdapter; // instance variable

  /**
   * obtain the id of the tweet to be displayed in the details view from the activity intent.
   * Iterate through the list of tweets to find a match.
   * Having found a matching residence, invoke viewPager.setCurrentItem, using the index position of the tweet object as an argument
   */
    @Override
    public void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);

      viewPager = new ViewPager(this); //Create and show ViewPager
      viewPager.setId(R.id.viewPager);
      setContentView(viewPager); //obtain the id
      setTweetList();

      pagerAdapter = new PagerAdapter(getSupportFragmentManager(), tweets);//Instantiate the pageAdapter
      viewPager.setAdapter(pagerAdapter); //Use the newly-created PageAdapter object as a parameter; iterate through the list of tweets
      viewPager.addOnPageChangeListener(this); //Register the OnPageListener
      setCurrentItem(); //having found a matching tweet, invoke viewPager.setCurrentItem using the index position of the tweet object as an argument
    }

  /**
   * private method to obtain a reference to the list of references stored in the model layer.
   * Facilitates retrieval of views that are required as a user swipes left or right
   */
    private void setTweetList() {
      MyTweetApp app = (MyTweetApp) getApplication();
      portfolio = app.portfolio;
      tweets = portfolio.tweets;
    }

    /*
    * Ensures the selected tweet is shown in details view.
    * Obtains the id of the tweet to be displayed in the details view from the activity intent,
    * iterates through the list of tweets to find a match.
    * Having found a matching tweet, invokes viewPager.setCurrentItem using the index position of the tweet object as an argument.
    */
    private void setCurrentItem() {
      Long tweetId = (Long) getIntent().getSerializableExtra(TweetFragment.EXTRA_TWEET_ID);
      for (int i = 0; i < tweets.size(); i++) {
        if (tweets.get(i).id.equals(tweetId)) {
          viewPager.setCurrentItem(i);
          break;
        }
      }
    }
  //------------------------------------------------ ViewPager ------------------------------------------------------------
  /**
   * ViewPager.OnPageChangeListener
   * Autogenerates these 3 methods:
   * o nly interested in full implementation of onPageScrolled
   */
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
      info(this, "onPageScrolled: position " + position + " arg1 " + positionOffset + " positionOffsetPixels " + positionOffsetPixels);
      Tweet tweet = tweets.get(position);
      if (tweet.content != null) {
        setTitle(tweet.content);
      }
    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

//============================================ PagerAdapter class ==========================================================
/**
 * The PagerAdapter class is only used by TweetPagerActivity, therefore a nested class.
 */

class PagerAdapter extends FragmentStatePagerAdapter
{
  private ArrayList<Tweet> tweets;

  public PagerAdapter(FragmentManager fm, ArrayList<Tweet> tweets) {
    super(fm);
    this.tweets = tweets;
  }

  @Override
  public int getCount() {
    return tweets.size();
  }

  //----------------------------------------------------------------------------------------------------
  /**
   * Adds a fragment argument to the Bundle object. The value of this argument is the current tweet id
   */
  @Override
  public Fragment getItem(int pos) {
    Tweet tweet = tweets.get(pos);
    Bundle args = new Bundle();
    args.putSerializable(TweetFragment.EXTRA_TWEET_ID, tweet.id);
    TweetFragment fragment = new TweetFragment();
    fragment.setArguments(args);
    return fragment;
  }
}
}


