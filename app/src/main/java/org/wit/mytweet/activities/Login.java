package org.wit.mytweet.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.wit.mytweet.R;
import org.wit.mytweet.app.MyTweetApp;
import org.wit.mytweet.models.User;

/**
 * Login class implements a listener interface for the OnClick events.
 */
public class Login extends AppCompatActivity implements View.OnClickListener {
    private User user;
    private TextView email;
    private TextView password;
    private Button loginButton;
    private MyTweetApp app;


  /**
   * Initializes the activity by calling setContentView(int) with a layout resource defining the UI
   * Listens for changes to the user input control:
   * When changes are detected (when a user enters data) the listener will receive the data and transmit it to the model class, User.
   * Uses the content auto-generated identifier(R.id.content) to obtain a reference to the control.
   */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginButton = (Button) findViewById(R.id.login);
        loginButton.setOnClickListener(this);

        app = (MyTweetApp) getApplication();
    }

  /**
   * If the user clicks on an item (in list), it starts the TweetList Activity view.
   */
 @Override
    public void onClick(View view) {

     email     = (TextView)  findViewById(R.id.loginEmail);
     password  = (TextView)  findViewById(R.id.loginPassword);

     if (app.validUser(email.getText().toString(), password.getText().toString())) {
         startActivity (new Intent(this, TweetListActivity.class));
     }
     else {
       // A toast is a short message that informs the user of something but does not require any input
       // Calls method from the Toast class: public static Toast makeText(Context context, int resld, int duration)
       // Context parameter is typically an instance of Activity (Activity is a subclass of Context).
       // The second parameter is the resource ID of the string that the toast should display
       // third parameter specifies how long the toast should be visible
         Toast toast = Toast.makeText(this, "Invalid Credentials", Toast.LENGTH_SHORT);
         toast.show();
     }
    }

    }
