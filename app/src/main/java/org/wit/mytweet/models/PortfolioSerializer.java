package org.wit.mytweet.models;

    import java.io.BufferedReader;
    import java.io.FileNotFoundException;
    import java.io.IOException;
    import java.io.InputStream;
    import java.io.InputStreamReader;
    import java.io.OutputStream;
    import java.io.OutputStreamWriter;
    import java.io.Writer;
    import java.util.ArrayList;

    import org.json.JSONArray;
    import org.json.JSONException;
    import org.json.JSONTokener;

    import android.content.Context;

/**
 * PortfolioSerializer model class to save / restore a list of tweets.
 * Serialization mechanism to save / restore the tweetlist to and from a file.
 * The app will load the contents of this file on launch and update the file if tweet data is updated.
 */
public class PortfolioSerializer
{
  private Context mContext;
  private String mFilename;

  /**
   * Constructor + 2 Methods fully Encapsulates the serialisation mechanisms
   * Overloaded Constructor
   */
  public PortfolioSerializer(Context c, String f)
  {
    mContext = c;
    mFilename = f;
  }

  /**
   * Create a JSONArray object
   * Place each tweet in turn into the object
   * Write the object to the file, Close the file.
   * If any exceptions occur, ‘propagate’ to the caller.
   */
  public void saveTweets(ArrayList<Tweet> tweets) throws JSONException, IOException
  {
    // build an array in JSON
    JSONArray array = new JSONArray();
    for (Tweet c : tweets) // Place each tweet in turn into the object
      array.put(c.toJSON());

    // write the file to disk
    Writer writer = null;
    try
    {
      OutputStream out = mContext.openFileOutput(mFilename, Context.MODE_PRIVATE);
      writer = new OutputStreamWriter(out);
      writer.write(array.toString());
    }
    finally
    {
      if (writer != null)
        writer.close();
    }
  }

  /**
   * Create Tweet Array
   * Attach a reader to the file (via a buffered reader)
   * Read the file into a string
   * Tokenize the string into individual json objects
   * Extract each object in turn and create a new Tweet Object from it
   * Add to the TweetList
   */
  public ArrayList<Tweet> loadTweets() throws IOException, JSONException {

    ArrayList<Tweet> tweets = new ArrayList<Tweet>(); // Create Residence Array
    BufferedReader reader = null; //Attach a reader to the file (via a buffered reader)
    try
    {
      // open and read the file into a StringBuilder
      InputStream in = mContext.openFileInput(mFilename);
      reader = new BufferedReader(new InputStreamReader(in));
      // Read the file into a string:
      StringBuilder jsonString = new StringBuilder();
      String line = null;
      while ((line = reader.readLine()) != null)
      {
        // line breaks are omitted and irrelevant
        jsonString.append(line);
      }
      // parse the JSON using JSONTokener (Tokenize the string into individual json objects)
      JSONArray array = (JSONArray) new JSONTokener(jsonString.toString()).nextValue();
      // build the array of residences from JSONObjects
      for (int i = 0; i < array.length(); i++)
      {
        // Extract each object in turn and create a new Tweet Object from it
        // Add to the TweetList
        tweets.add(new Tweet(array.getJSONObject(i)));
      }
    }
    catch (FileNotFoundException e)
    {
      // we will ignore this one, since it happens when we start fresh
    }
    finally
    {
      if (reader != null)
        reader.close();
    }
    return tweets;
  }


}