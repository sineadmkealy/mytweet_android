package org.wit.mytweet.models;

import java.util.ArrayList;

import android.util.Log;

import static org.wit.android.helpers.LogHelpers.info;

/**
 *  The Portfolio class uses the PortfolioSerializer class to save / restore the Tweets it is managing,
 *  The PortfolioSerializer class to read and write the tweet list.
 */
public class Portfolio {
  public ArrayList<Tweet> tweets;
  private PortfolioSerializer   serializer;

  /**
   * Reads the tweet list - in the constructor when Portfolio created
   */
  public Portfolio(PortfolioSerializer serializer) {
    this.serializer = serializer;
    try {
      tweets = serializer.loadTweets();
    } catch (Exception e) {
      info(this, "Error loading residences: " + e.getMessage());
      tweets = new ArrayList<Tweet>();
    }
  }

  /**
   * Adds a tweet to the tweet list
   */
  public void addTweet(Tweet tweet) {

    tweets.add(tweet);
  }

  public Tweet getTweet(Long id) {
    Log.i(this.getClass().getSimpleName(), "Long parameter id: " + id);

    for (Tweet tweet : tweets) {
      if (id.equals(tweet.id)) {
        return tweet;
      }
    }
    info(this, "failed to find tweet. Returning first element array to avoid crash");
    return null;
  }

  /**
   * Deletes a tweet from the tweet list (Context Menu)
   */
  public void deleteTweet(Tweet tweet)
  {
    tweets.remove(tweet);
    saveTweets();
  }

  /**
   * Maps to menu Tweetlist_Context.
   */
  public void clearAllTweets()
  {
    tweets.clear();
    saveTweets();
  }

  /**
   * A method to save all the residence to disk
   * Writes the tweet list, called when data may have changed
   */
  public boolean saveTweets() {
    try
    {
      serializer.saveTweets(tweets);
      info(this, "Tweets saved to file");
      return true;
    }
    catch (Exception e)
    {
      info(this, "Error saving tweets: " + e.getMessage());
      return false;
    }
  }
}
