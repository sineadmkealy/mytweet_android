package org.wit.mytweet.models;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;
import org.wit.mytweet.R;

import java.util.Date;
import java.util.Random;

/**
 * The Tweet model class uses the JSON format as a serialization mechanism to save / restore the TweetList to and from a file.
 * The app will load the contents of this file on launch and update the file if tweet data is updated.
 *
 * JSON is a language for representing simple data structures and associative arrays, called objects.
 * This format used for serializing and transmitting structured data over a network connection. It is used
 * primarily to transmit data between a server and web application, an alternative to XML.
 */
public class Tweet {

  public Long id;
  public Long date;
  public String content;
  public String contact;

  //
  //Define JSON string IDs for each field :  keeps serialization on track.
  private static final String JSON_ID = "id";
  private static final String JSON_CONTENT = "content";
  private static final String JSON_DATE = "date";
  private static final String JSON_CONTACT = "contact";


  /**
   * Default Constructor
   */
  public Tweet() {
    id = unsignedLong(); //Protects against a negative id
    id = new Random().nextLong();
    date = new Date().getTime();
   // content = "What's up?";
   // contact = "none presently"; // String literal to avoid a future null pointer exception error
  }

  /**
   * Overloaded  constructor to load a tweet object from JSON.
   * Reads a json Tweet Object
   */
  public Tweet(JSONObject json) throws JSONException {
    id = json.getLong(JSON_ID);
    content = json.getString(JSON_CONTENT);
    date = json.getLong(JSON_DATE);
    contact = json.getString(JSON_CONTACT);
  }

  /**
   * The serialization itself: corresponding method to save an object to JSON.
   * Writes a json Tweet Object
   */
  public JSONObject toJSON() throws JSONException {
    JSONObject json = new JSONObject();
    json.put(JSON_ID, Long.toString(id));
    json.put(JSON_CONTENT, content);
    json.put(JSON_DATE, date);
    json.put(JSON_CONTACT, contact);
    return json;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getContent() {
    return content;
  }

  public String getDateString() {
    return "Tweeted on:" + dateString();
  }

  private String dateString() {
    String dateFormat = "EEE d MMM yyyy H:mm";
    return android.text.format.DateFormat.format(dateFormat, date).toString();
  }

  /**
   * Method to generate the contents of the email
   */
  public String getTweetEmail(Context context) {
    String Contact = contact;
    if (contact == null) {
      Contact = context.getString(R.string.tweet_email_no_contact_exists);
    } else {
      Contact = context.getString(R.string.tweet_email_contact, contact);
    }
    String email = content;
    return email;
  }


    /**
     * Generate a long greater than zero
     * @return Unsigned Long value greater than zero
     */
    private Long unsignedLong() {
        long rndVal = 0;
        do {
            rndVal = new Random().nextLong();
        } while (rndVal <= 0);
        return rndVal;
    }
}
