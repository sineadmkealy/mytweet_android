package org.wit.mytweet.app;

import android.app.Application;
import android.util.Log;
import android.widget.Toast;

import org.wit.mytweet.models.Portfolio;
import org.wit.mytweet.models.PortfolioSerializer;
import org.wit.mytweet.models.Tweet;
import org.wit.mytweet.models.User;

import java.util.ArrayList;
import java.util.List;

import static org.wit.android.helpers.LogHelpers.info;


/**
 * The entry point for the application.
 * The pplication object manages the list of tweets.
 * This is an object created when the application is launched. This is unique.
 * Creates the single Portfolio object, which contains the list of tweets
 * the app is managing.
 */
public class MyTweetApp extends Application {

  private static final String FILENAME = "portfolio.json";//Creates the serializer, giving it a file name to use:
  public Portfolio portfolio;
  public static MyTweetApp app; //refactoring TweetActivity for fragments
  public ArrayList<User> users = new ArrayList<User>();

  /**
   * Passes the serialiser to the Portfolio object.
   * The Portfolio object manages loading / saving to the file using the serializer
   */
  @Override
  public void onCreate() {
    super.onCreate();

    PortfolioSerializer serializer = new PortfolioSerializer(this, FILENAME);
    portfolio = new Portfolio(serializer);

    app = this;// initialize
    info(this, "MyTweet app launched");
  }

  /**
   * Creates new user
   */
  public void newUser(User user) {

    users.add(user);
    Log.v("check", user.firstName + " " + user.lastName);
  }

  /**
   * Login method to authenticate the user
   */
  public boolean validUser(String email, String password) {
    Log.v("check", "validUser method");
    for (User user : users) {
      if (user.email.equals(email) && user.password.equals(password)) {
        return true;
      }
    }
    return false;
  }

  /**
   * getter for protected MyRentApp field
   */
  public static MyTweetApp getApplication() {
    return app;
  }

  /**
   * Introduced Story_07 to introduce activity-fragment pairs
   * (required for horizontal swipe)
   */
  public static MyTweetApp getApp() {
    return app;
  }
}