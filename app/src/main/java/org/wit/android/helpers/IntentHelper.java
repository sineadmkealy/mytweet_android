package org.wit.android.helpers;

import android.app.Activity;
import android.content.Intent;
import android.provider.ContactsContract;
import android.content.Context;
import android.net.Uri;
import android.support.v4.app.NavUtils;

import java.io.Serializable;

/**
 * Implicit Intents
 * An app may also want to perform some action, such as send an email, text message, or status update.
 * <p>
 * If the application does not have its own activities to perform such actions, leverage the activities
 * provided by other applications on the device, which have declared (using intent filers) that can perform
 * the actions
 * <p>
 * If there are multiple activities that can handle the intent, then the user can select which one to use
 */
public class IntentHelper {
    /**
     *Start an activity by calling startActivity(), passing it an Intent that
     * 1. The intent specifies either the exact activity you want to start or
     * 2. describes the type of action you want to perform (and the system selects
     * the appropriate activity for you, which can even be from a different application)
     *
     * 3. An intent can also carry small amounts of data to be used by the activity that is started
     */

    /**
     * Starting an activity for a result:
     * start the activity by calling startActivityForResult() (instead of startActivity())
     * To then receive the result from the subsequent activity, implement the onActivityResult() callback method
     * When the subsequent activity is done, it returns a result in an Intent to your onActivityResult() method
     */
    public static void startActivity(Activity parent, Class classname) {
        Intent intent = new Intent(parent, classname);
        parent.startActivity(intent);
    }

    public static void startActivityWithData(Activity parent, Class classname, String extraID, Serializable extraData) {
        Intent intent = new Intent(parent, classname);
        intent.putExtra(extraID, extraData);
        parent.startActivity(intent);
    }

    public static void startActivityWithDataForResult(Activity parent, Class classname, String extraID, Serializable extraData, int idForResult) {
        Intent intent = new Intent(parent, classname);
        intent.putExtra(extraID, extraData);
        parent.startActivityForResult(intent, idForResult);
    }

    public static void navigateUp(Activity parent)
    {
        Intent upIntent = NavUtils.getParentActivityIntent(parent);
        NavUtils.navigateUpTo(parent, upIntent);
    }

  /**
   * method to trigger contact list and email access:
   */
    public static void selectContact(Activity parent, int id)
    {
        Intent selectContactIntent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        parent.startActivityForResult(selectContactIntent, id);
    }

}